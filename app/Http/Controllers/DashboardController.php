<?php

namespace App\Http\Controllers;

use App\Models\Bulan;
use App\Models\Keluar;
use App\Models\Pelanggan;
use App\Models\User;

class DashboardController extends Controller
{
    public function dashboard(){
        $bulan=Bulan::all();
        $user=User::all();
        return view('dashboard.dashboardringkas',['bulan'=>$bulan,'user'=>$user]);
    }
    public function pindah($id){
        $pindah=User::where('id_user','=',$id)->get();
        $daftar=Pelanggan::where('id_user','=',$id)->get();
        $keluar=Keluar::create([
            'id_user'=>$pindah[0]->id_user,
            'nama'=>$pindah[0]->name,
            'email'=>$pindah[0]->email,
            'masuk'=>$daftar[0]->tgl_daftar,
            'keluar'=>date('Y/m/d')
        ]);
        
        $user=User::where('id_user','=',$id)->delete();
        if($keluar){
            if($user){
                return redirect('/dashboard')->with('sukses','Penghuni Kost Telah Keluar');
            }
        }
    }
    
}
