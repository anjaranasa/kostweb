<?php

namespace App\Http\Controllers;

use App\Mail\ReminderMail;
use App\Models\Bayar;
use App\Models\Bulan;
use App\Models\Pendapatan;
use App\Models\Tahun;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;


class InputController extends Controller
{
    public function input(){
        $tahun=Tahun::where('tahun','=',date('Y'))->get();
        $bulan1=Bulan::where('bulan','=',date('M'))->get();
        $id_tahun=Str::random(12);
        if ($tahun->isEmpty()){
            
            $intahun=Tahun::create([
                'id_tahun'=>$id_tahun,
                'tahun'=>date('Y')
            ]);
            
            if($intahun){
                $id_bulan=Str::random(9);
                $bulan=Bulan::create([
                    'id_bulan'=> $id_bulan,
                    'id_tahun'=>$id_tahun,
                    'bulan'=>date('M')
                ]);
                if($bulan){
                    $user=User::all();
                    foreach($user as $users){
                        $id_bayar=Str::random(8);
                        if($users['role']==2){
                            $create=$users->created_at;
                            $tanggal=new Carbon($create);
                            $deadline=$tanggal->addDays(30);
                            $bayar=Bayar::create([
                                'id_user'=> $users->id_user,
                                'id_bulan'=>$id_bulan,
                                'id_tahun'=>$id_tahun,
                                'id_bayar'=>$id_bayar,
                                'deadline'=>$deadline,
                                'awal'=>date('Y-m-d'),
                                'jlh_bayar'=> 450000,
                                'status'=> 'Belum Lunas'
                            ]);
                            $user=User::where('id_user','=',$users->id_user)->update([
                                'created_at'=>$deadline
                            ]);
                        }
                    }
                    if($bayar){
                        if($user){
                            return redirect('/input');
                        }
                    }
                }
            }
        }else{
            $id_bulan=Str::random(9);
            
            if(!$bulan1->isEmpty()){
                return redirect('/input')->with('ada','Bulan Sudah Ada');
            }else{
                $bulan=Bulan::create([
                    'id_bulan'=> $id_bulan,
                    'id_tahun'=>$tahun[0]->id_tahun,
                    'bulan'=>date('M')
                ]);
                if($bulan){
                    $user=User::all();
                    foreach($user as $users){
                        $id_bayar=Str::random(8);
                        if($users['role']==2){
                            $create=$users->created_at;
                            $tanggal=new Carbon($create);
                            $deadline=$tanggal->addDays(30);
                            
                            $bayar=Bayar::create([
                                'id_user'=> $users->id_user,
                                'id_bulan'=>$id_bulan,
                                'id_tahun'=>$tahun[0]->id_tahun,
                                'id_bayar'=>$id_bayar,
                                'deadline'=>$deadline,
                                'awal'=>date('Y-m-d'),
                                'jlh_bayar'=> 450000,
                                'status'=> 'Belum Bayar'
                            ]);
                            $user=User::where('id_user','=',$users->id_user)->update([
                                'created_at'=>$deadline
                            ]);
                        }
                    }
                    if($bayar){
                        if($user){
                            return redirect('/input');
                        }
                    }
                }
            }
        }
    }
    public function updateBayar($id){
        
        $bayar=Bayar::where('id_bayar','=',$id)->get();
        $bulan=Bulan::where('id_bulan','=',$bayar[0]->id_bulan)->get();
        $user=User::where('id_user','=',$bayar[0]->id_user)->get();
        $condition=true;
        $insertpendapatan=Pendapatan::create([
            'jumlah'=>$bayar[0]->jlh_bayar,
            'id_tahun'=>$bayar[0]->id_tahun,
            'id_bulan'=>$bayar[0]->id_bulan
        ]);
        $updatebayar=Bayar::where('id_bayar','=',$id)
        ->update([
            'tanggal_bayar'=>date('Y-m-d'),
            
        ]);
        $data=Bayar::where('id_bayar','=',$id)->get();
        
        Mail::to($user[0]->email)->send(new ReminderMail($data,$user,$bulan,$condition));
        if($insertpendapatan){
            if($updatebayar){
                session()->flash('sukses','Konfirmasi Sudah Sukses');
            }
        }
    }
}
