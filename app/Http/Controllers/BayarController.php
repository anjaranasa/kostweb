<?php

namespace App\Http\Controllers;

use App\Models\Bayar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BayarController extends Controller
{
    // Set your Merchant Server Key
    public $snapToken;
    public function index($id){
        
        $bayar=Bayar::where('id_bayar','=',$id)->get();
        
        if(isset($_GET['result'])){
            
            $status=json_decode($_GET['result'],true);
            $id_bayar=$status['order_id'];
            $time=$status['transaction_time'];
            Bayar::where('id_bayar','=',$id_bayar)->update([
                'status'=>"Dalam Proses",
                'tanggal_bayar'=>date('Y-m-d'),
                'method'=>$status['payment_type'],
                'dl_bayar'=>date('Y-m-d H:i:s',strtotime('+1 day',strtotime($time))),
                'bank'=>$status['va_numbers'][0]['bank'],
                'va'=>$status['va_numbers'][0]['va_number'],
                'pdf'=>$status['pdf_url']
            ]);
            session()->flash('sudah', 'Task was successful!');
        }
        else{
            $bayar=Bayar::where('id_bayar','=',$id)->get();
        }
        if(!$bayar->isEmpty()){
            if ($bayar[0]->status=="Belum Bayar"){
            
                \Midtrans\Config::$serverKey = 'SB-Mid-server-ZEJZWgdliopUKuEZFZvsYs8F';
                // Set to Development/Sandbox Environment (default). Set to true for Production Environment (accept real transaction).
                \Midtrans\Config::$isProduction = false;
                // Set sanitization on (default)
                \Midtrans\Config::$isSanitized = true;
                // Set 3DS transaction for credit card to true
                \Midtrans\Config::$is3ds = true;
                
                $params = array(
                    'transaction_details' => array(
                        'order_id' => $bayar[0]->id_bayar,
                        'gross_amount' => $bayar[0]->jlh_bayar,
                    ),
                    'customer_details' => array(
                        'first_name' => 'Kepada',
                        'last_name' => Auth::user()->name,
                        'email' => Auth::user()->email,
                        'phone' => '08111222333',
                    ),
                );
                
                $this->snapToken = \Midtrans\Snap::getSnapToken($params);
                session()->flash('bayar', 'Task was successful!');
                return view('user.tombolbayar',['token'=>$this->snapToken,'status'=>$bayar[0]->status]);
                
                
            }else if($bayar[0]->status=="Dalam Proses"){
                
                return view('user.tombolbayar',['bayar1'=>$bayar,'token'=>$this->snapToken]);
                dd($bayar);
                
                
                //dd($status);
            }
            
        }
        else{
            
            return view('user.tombolbayar',['token'=>$this->snapToken,'bayar1'=>$bayar]);
        }
        
    }
}
