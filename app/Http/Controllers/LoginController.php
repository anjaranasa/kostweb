<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function login(Request $request){
        $a=Auth::attempt($request->only('email','password'));
        $role=Auth::user()->role;

        if($a){
            if($role==1){
                return redirect('/dashboard');
            }
            else{
                return redirect('/user');
            }
        }
    }
    public function logout(){
        Auth::logout();
        return redirect('/');
    }
    
}
