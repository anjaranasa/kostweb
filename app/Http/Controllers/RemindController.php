<?php

namespace App\Http\Controllers;

use App\Mail\ReminderMail;
use App\Models\Bayar;
use App\Models\Bulan;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class RemindController extends Controller
{
    public function index(){
        $user=User::all();
        $bayar=Bayar::all();
        foreach($user as $users){
            if($users->role==2){
                foreach($bayar as $bayars){
                    $data=DB::table('pembayaran')->
                    join('pelanggan','pembayaran.id_user','=','pelanggan.id_user')->
                    where('pembayaran.id_user','=',$bayars->id_bayar)->get();
                    $bulan=Bulan::where('id_bulan','=',$data[0]->id_bulan);
                
                    Mail::to($users->email)->send(new ReminderMail($data));
                }
                
                
            }
        }
        
    }
}
