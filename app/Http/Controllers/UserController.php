<?php

namespace App\Http\Controllers;

use App\Models\Bayar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    public function tagihan(){
        $id_user=Auth::user()->id_user;
        $bayar=DB::table('pembayaran')
        ->join('bulan','pembayaran.id_bulan','=','bulan.id_bulan')
        ->where('pembayaran.id_user','=',$id_user)
        ->get();
        return view('user.dashboarduser',['bayar'=>$bayar]);
    }
}
