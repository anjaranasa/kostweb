<?php

namespace App\Http\Controllers;

use App\Models\Keluar;
use App\Models\User;
use Illuminate\Http\Request;

class UserKeluarController extends Controller
{
    public function index(){
        $keluar=Keluar::all();
        $user=User::all();
        return view('userkeluar',['keluar'=>$keluar]);
    }
}
