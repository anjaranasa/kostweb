<?php

namespace App\Http\Controllers;

use App\Models\Pelanggan;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class RegisterController extends Controller
{
    public function regis(Request $req){
        $id_user="ID".date('dmY')."".Str::random(4);
        $user=User::create([
            'name' => $req->name,
            'email'=>$req->email,
            'id_user'=>$id_user,
            'role'=>2,
            'password'=>bcrypt($req->password)
        ]);
        $pelanggan=Pelanggan::create([
            'id_user'=> $id_user,
            'nama'=>$req->name,
            'univ'=>$req->univ,
            'tgl_daftar'=>date('Y-m-d')
        ]);
        if($user){
            if($pelanggan){
                return redirect('/');
            }
        }
    }
}
