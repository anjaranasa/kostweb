<?php

use App\Http\Controllers\BayarController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\InfoController;
use App\Http\Controllers\InputController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\RemindController;
use App\Http\Controllers\SummaryController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\UserKeluarController;
use Illuminate\Auth\Events\Login;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
})->name('login');
Route::post('/login',[LoginController::class,'login']);
Route::get('/regis', function () {
    return view('register');
});
Route::post('/regis/reg',[RegisterController::class,'regis']);
Route::group(['middleware'=>['auth','role:1']],function(){
    Route::get('/dashboard',[DashboardController::class,'dashboard']);
    Route::get('/pindah/{id}',[DashboardController::class,'pindah']);
    Route::get('/info',[InfoController::class,'tahun']);
    Route::get('input',function(){
        return view('inputtag');
    });
    Route::get('/databayar',function(){
        return view('databayar');
    }); 
    Route::get('/remind',function(){
        return view('reminder.remind');
    }); 
    Route::get('/bulan/{id}',[InfoController::class,'index']);
    Route::post('/input/in',[InputController::class,'input']);
    Route::get('/logout',[LoginController::class,'logout']);
    Route::get('/detail/{id}',[InfoController::class,'detail']);
    Route::get('/status/{id}',[InputController::class,'updateBayar']);
    Route::get('/remind/reminder/{id}',[InfoController::class,'reminder']);
    Route::get('/userkeluar',[UserKeluarController::class,'index']);
});
Route::group(['middleware'=>['auth','role:2']],function(){
    Route::get('/user',[UserController::class,'tagihan']);
    Route::get('/logout1',[LoginController::class,'logout']);
    Route::get('/bayar/{id}',[BayarController::class,'index']);
});


