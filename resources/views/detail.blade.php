<!--

=========================================================
* Now UI Dashboard - v1.5.0
=========================================================

* Product Page: https://www.creative-tim.com/product/now-ui-dashboard
* Copyright 2019 Creative Tim (http://www.creative-tim.com)

* Designed by www.invisionapp.com Coded by www.creative-tim.com

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

-->
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="{{asset('assets/img/apple-icon.png')}}">
  <link rel="icon" type="image/png" href="{{asset('assets/img/favicon.png')}}">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    Now UI Dashboard by Creative Tim
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
  <!-- CSS Files -->
  <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" />
  <link href="{{asset('assets/css/now-ui-dashboard.css?v=1.5.0')}}" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="{{asset('assets/demo/demo.css')}}" rel="stylesheet" />
</head>

<body class="">
  <div class="wrapper ">
    <div class="sidebar" data-color="orange">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red | yellow"
    -->
      <div class="logo">
        <a href="http://www.creative-tim.com" class="simple-text logo-mini">
          CT
        </a>
        <a href="http://www.creative-tim.com" class="simple-text logo-normal">
          Creative Tim
        </a>
      </div>
      <div class="sidebar-wrapper" id="sidebar-wrapper">
        <ul class="nav">
          <li>
            <a href="/dashboard">
              <i class="now-ui-icons design_app"></i>
              <p>Dashboard</p>
            </a>
          </li>
          <li class="active ">
            <a href="/info">
              <i class="now-ui-icons shopping_bag-16"></i>
              <p>Data</p>
            </a>
          </li>
          <li>
            <a href="/userkeluar">
              <i class="now-ui-icons ui-1_simple-add"></i>
              <p>User Keluar</p>
            </a>
          </li>
          <li>
            <a href="/input">
              <i class="now-ui-icons ui-1_simple-add"></i>
              <p>Input Tagihan</p>
            </a>
          </li>
          <li>
            <a href="/logout">
              <i class="now-ui-icons arrows-1_minimal-left"></i>
              <p>Logout</p>
            </a>
          </li>
        </ul>
      </div>
    </div>
    <div class="main-panel" id="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent  bg-primary  navbar-absolute">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <div class="navbar-toggle">
              <button type="button" class="navbar-toggler">
                <span class="navbar-toggler-bar bar1"></span>
                <span class="navbar-toggler-bar bar2"></span>
                <span class="navbar-toggler-bar bar3"></span>
              </button>
            </div>
            <a class="navbar-brand" href="#pablo">Data Penghuni Kost</a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-bar navbar-kebab"></span>
            <span class="navbar-toggler-bar navbar-kebab"></span>
            <span class="navbar-toggler-bar navbar-kebab"></span>
          </button>
          
        </div>
      </nav>
      <!-- End Navbar -->
      <div class="panel-header panel-header-sm">
      </div>
      <div class="content">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title"> Detai Tagihan</h4>
              </div>
              @if(session('sukses'))
                <div class="m-3 alert alert-success">
                  {{session('sukses')}}
                </div>
              @endif
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table">
                  <th>Nama</th>
                    <th>Id Bayar</th>
                    <th>Bulan</th>
                    <th>Jumlah Bayar</th>
                    <th>Tanggal Bayar</th>
                    <th>Status</th>
                    <th>Dibuat Pada</th>
                    <th>Deadline Bayar</th>
                    <th>Aksi</th>
                    @foreach($bayar as $bayars)
                    <tr>
                        <td>@php 
                             foreach($nama as $a){
                                 if ($bayars->id_user==$a->id_user){
                                     echo $a->nama;
                                 }
                             }

                            @endphp
                        </td>
                        <td>
                            {{$bayars->id_bayar}}
                        </td>
                        <td>
                            {{$bayars->bulan}}
                        </td>
                        <td>
                            @currency($bayars->jlh_bayar)
                        </td>
                        <td>
                        @php
                             if($bayars->tanggal_bayar==null){

                             }else{
                               $date=date('d M Y',strtotime($bayars->tanggal_bayar));
                               echo $date;
                             }
                        @endphp
                            
                        </td>
                        <td>
                        @if($bayars->status=="Sudah Bayar")
                            <div class="text-success">
                            {{$bayars->status}}
                            </div>
                        @else
                            <div class="text-danger">
                            {{$bayars->status}}
                            </div>
                        @endif
                        </td>
                        <td>
                             @php
                             $date1=date('d M Y',strtotime($bayars->awal));
                             echo $date1;
                              
                             @endphp
                        </td>
                        <td>
                            <?php
                            if($bayars->tanggal_bayar==null){
                              $date=date('m/d/Y',strtotime($bayars->deadline));
                                $datenow=date('m/d/Y');
                                
                                $datecrea=date_create($date);
                                $datecrea1=date_create($datenow);
                                
                                $selish=date_diff($datecrea1,$datecrea);
                                $data=$selish->days;
                                $date=date('d M Y',strtotime($bayars->deadline));
                                if($data>20){
                                  ?>
                                  <div class="text-success">
                                    {{$date}}
                                  </div>
                                  <?php
                                }elseif($data>10){
                                  ?>
                                  <div class="text-warning">
                                  {{$date}}
                                  </div>
                                  <?php
                                }
                                else{
                                  ?>
                                  <div class="text-danger">
                                  {{$date}}
                                  </div>
                                  <?php
                                }
                            }
                            else{
                              $date1=date('d M Y',strtotime($bayars->deadline));
                              echo $date1;
                            }  
                            ?>
                            
                        </td>
                        <td>
                        @if($bayars->status=="Dalam Proses")
                        <a class="btn btn-sm btn-primary" href="/status/{{$bayars->id_bayar}}">Bayar</a>
                        @else
                        @endif
                        <a href="/remind/reminder/{{$bayars->id_bayar}}" class="btn btn-sm btn-warning">Reminder</a></td>
                    </tr>
                    @endforeach
                  </table>
                </div>
              </div>
            </div>
          </div>
          
        </div>
      </div>
      <footer class="footer">
        <div class=" container-fluid ">
          <nav>
            <ul>
              <li>
                <a href="https://www.creative-tim.com">
                  Creative Tim
                </a>
              </li>
              <li>
                <a href="http://presentation.creative-tim.com">
                  About Us
                </a>
              </li>
              <li>
                <a href="http://blog.creative-tim.com">
                  Blog
                </a>
              </li>
            </ul>
          </nav>
          <div class="copyright" id="copyright">
            &copy; <script>
              document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))
            </script>, Designed by <a href="https://www.invisionapp.com" target="_blank">Invision</a>. Coded by <a href="https://www.creative-tim.com" target="_blank">Creative Tim</a>.
          </div>
        </div>
      </footer>
    </div>
  </div>
  <!--   Core JS Files   -->
  <script src="{{asset('assets/js/core/jquery.min.js')}}"></script>
  <script src="{{asset('assets/js/core/popper.min.js')}}"></script>
  <script src="{{asset('assets/js/core/bootstrap.min.js')}}"></script>
  <script src="{{asset('assets/js/plugins/perfect-scrollbar.jquery.min.js')}}"></script>
  <!--  Google Maps Plugin    -->
  <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
  <!-- Chart JS -->
  <script src="{{asset('assets/js/plugins/chartjs.min.js')}}"></script>
  <!--  Notifications Plugin    -->
  <script src="{{asset('assets/js/plugins/bootstrap-notify.js')}}"></script>
  <!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="{{asset('assets/js/now-ui-dashboard.min.js?v=1.5.0')}}" type="text/javascript"></script><!-- Now Ui Dashboard DEMO methods, don't include it in your project! -->
  <script src="{{asset('assets/demo/demo.js')}}"></script>
</body>

</html>