-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 18, 2022 at 10:52 AM
-- Server version: 8.0.24
-- PHP Version: 8.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kost`
--

-- --------------------------------------------------------

--
-- Table structure for table `bulan`
--

CREATE TABLE `bulan` (
  `id` bigint UNSIGNED NOT NULL,
  `id_bulan` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `id_tahun` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `bulan` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `hhff` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `bulan`
--

INSERT INTO `bulan` (`id`, `id_bulan`, `id_tahun`, `bulan`, `created_at`, `updated_at`, `hhff`) VALUES
(2, 'RFsLgU1eJ', 'sfggegehhaw', 'Januari', '2021-04-06 07:27:49', '2021-04-06 07:27:49', NULL),
(3, 'w0djk3o7A', 'sfggegehhaw', 'Februari', '2021-04-06 18:57:15', '2021-04-06 18:57:15', NULL),
(5, 'BwxRd0cyt', 'sfggegehhaw', 'Maret', '2021-04-06 19:20:33', '2021-04-06 19:20:33', NULL),
(21, '2MoFnbmzB', 'sfggegehhaw', 'Apr', '2021-04-09 21:55:39', '2021-04-09 21:55:39', NULL),
(22, 'nLMV0NwBY', 'sfggegehhaw', 'Mei', '2021-04-14 20:07:54', '2021-04-14 20:07:54', NULL),
(29, 'rRfWAzD7y', 'sfggegehhaw', 'Jun', '2021-06-17 23:12:13', '2021-06-17 23:12:13', NULL),
(33, 'Nr25qn6dY', 'sfggegehhaw', 'Jul', '2021-07-14 19:33:10', '2021-07-14 19:33:10', NULL),
(34, '6RgBhdoVh', 'sfggegehhaw', 'Sep', '2021-09-18 08:35:35', '2021-09-18 08:35:35', NULL),
(35, 'r4DGTANeY', 'sfggegehhaw', 'Oct', '2021-10-14 22:37:06', '2021-10-14 22:37:06', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint UNSIGNED NOT NULL,
  `uuid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `connection` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `queue` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int UNSIGNED NOT NULL,
  `migration` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `batch` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2021_04_06_100614_create_bulan_table', 1),
(5, '2021_04_06_100711_create_pembayaran_table', 1),
(6, '2021_04_06_101931_create_pelanggan_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- --------------------------------------------------------

--
-- Table structure for table `pelanggan`
--

CREATE TABLE `pelanggan` (
  `id` bigint UNSIGNED NOT NULL,
  `id_user` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `nama` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `univ` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `tgl_daftar` date DEFAULT NULL,
  `tgl_keluar` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `pelanggan`
--

INSERT INTO `pelanggan` (`id`, `id_user`, `nama`, `univ`, `tgl_daftar`, `tgl_keluar`, `created_at`, `updated_at`) VALUES
(1, 'oys6bls1rFf', 'Anjar', 'Polines', '2021-04-01', NULL, '2021-04-06 07:02:47', '2021-04-06 07:02:47'),
(2, '1w57dcLkxaS', 'Adit', 'Polines', '2021-04-02', NULL, '2021-04-06 18:45:00', '2021-04-06 18:45:00'),
(3, 'ID10042021v8Qo', 'Anass', 'Polines', '2021-04-10', NULL, '2021-04-09 18:11:37', '2021-04-09 18:11:37'),
(4, 'ID150420210BAO', 'guest', 'Undip', '2021-04-15', NULL, '2021-04-14 21:32:40', '2021-04-14 21:32:40'),
(5, 'OYXQLFGlrKYj', 'Sugiyo', 'UNAIR', '2021-07-12', NULL, '2021-07-12 05:20:35', '2021-07-12 05:20:35'),
(6, '2lfHzt5H4uaC', 'AbdillahAnjar', 'UI', '2021-07-12', NULL, '2021-07-12 05:24:46', '2021-07-12 05:24:46');

-- --------------------------------------------------------

--
-- Table structure for table `pembayaran`
--

CREATE TABLE `pembayaran` (
  `id` bigint UNSIGNED NOT NULL,
  `id_user` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `id_bayar` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `id_bulan` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `id_tahun` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `jlh_bayar` int NOT NULL,
  `tanggal_bayar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `deadline` date DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `method` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `dl_bayar` datetime DEFAULT NULL,
  `bank` varchar(30) DEFAULT NULL,
  `va` varchar(30) DEFAULT NULL,
  `pdf` varchar(1000) DEFAULT NULL,
  `awal` date DEFAULT NULL,
  `trx_id` varchar(30) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `pembayaran`
--

INSERT INTO `pembayaran` (`id`, `id_user`, `id_bayar`, `id_bulan`, `id_tahun`, `jlh_bayar`, `tanggal_bayar`, `deadline`, `status`, `method`, `dl_bayar`, `bank`, `va`, `pdf`, `awal`, `trx_id`, `created_at`, `updated_at`) VALUES
(2, 'oys6bls1rFf', 'pxvtIjLw1', 'RFsLgU1eJ', NULL, 450000, '2021-04-30', NULL, 'Dalam Proses', 'bank_transfer', '2021-05-01 20:55:47', 'bca', '86214140594', 'https://app.sandbox.midtrans.com/snap/v1/transactions/445f8fbf-49e9-4619-96ab-5d100b7ede13/pdf', NULL, NULL, '2021-04-06 07:27:49', '2021-04-30 06:55:56'),
(3, 'oys6bls1rFf', 'IuGYiGQ41', 'w0djk3o7A', NULL, 450000, '2021-04-30', NULL, 'Dalam Proses', 'bank_transfer', '2021-05-01 21:01:46', 'bca', '86214354380', 'https://app.sandbox.midtrans.com/snap/v1/transactions/8df36366-9967-43e5-b5c3-1623488179fb/pdf', NULL, NULL, '2021-04-06 18:57:15', '2021-04-30 07:01:52'),
(6, 'oys6bls1rFf', 'hlgJ9zIa1', 'BwxRd0cyt', NULL, 450000, '2021-05-24', NULL, 'Dalam Proses', 'bank_transfer', '2021-05-25 14:02:26', 'bca', '86214732647', 'https://app.sandbox.midtrans.com/snap/v1/transactions/d3f73f70-7a77-4638-a671-119bb4864158/pdf', NULL, NULL, '2021-04-06 19:20:33', '2021-05-24 00:02:28'),
(7, '1w57dcLkxaS', 'ToVb3EAd', 'BwxRd0cyt', NULL, 450000, NULL, NULL, 'Belum Bayar', '', NULL, NULL, NULL, NULL, NULL, NULL, '2021-04-06 19:20:33', '2021-04-11 21:43:07'),
(8, 'oys6bls1rFf', 'BMoAP6UM', 'RrRg37oYa', NULL, 450000, NULL, NULL, 'Belum Bayar', '', NULL, NULL, NULL, NULL, NULL, NULL, '2021-04-06 23:49:14', '2021-04-06 23:49:14'),
(9, '1w57dcLkxaS', 'AqRqOt6S', 'RrRg37oYa', NULL, 450000, NULL, NULL, 'Belum Bayar', '', NULL, NULL, NULL, NULL, NULL, NULL, '2021-04-06 23:49:14', '2021-04-06 23:49:14'),
(27, 'oys6bls1rFf', 'QPlmkZcH1', '2MoFnbmzB', 'sfggegehhaw', 450000, '2021-05-24', '2021-05-10', 'settlement', 'bank_transfer', '2021-05-25 14:04:30', 'bca', '86214835478', 'https://app.sandbox.midtrans.com/snap/v1/transactions/161d04cf-17db-4e5a-be80-5a583a768b03/pdf', NULL, NULL, '2021-04-09 21:55:39', '2021-06-08 04:41:13'),
(28, '1w57dcLkxaS', 'e5f7ffcd-4173-49e2-9eb2-112ddfcc4d23', '2MoFnbmzB', 'sfggegehhaw', 450000, '2021-06-07', '2021-05-10', 'settlement', 'bank_transfer', '2021-06-08 09:46:41', 'bca', '86214579715', 'https://app.sandbox.midtrans.com/snap/v1/transactions/598ff8d8-8baa-48a2-9049-44cbdf6ec886/pdf', NULL, NULL, '2021-04-09 21:55:39', '2021-06-06 20:25:35'),
(29, 'ID10042021v8Qo', 'f663HwUg', '2MoFnbmzB', 'sfggegehhaw', 450000, '2021-06-06', '2021-05-10', 'Dalam Proses', 'bank_transfer', '2021-06-07 19:09:27', 'bca', '86214221339', 'https://app.sandbox.midtrans.com/snap/v1/transactions/dc48aa92-9be3-45e9-a652-ff14a74b2805/pdf', NULL, NULL, '2021-04-09 21:55:39', '2021-06-06 05:10:15'),
(30, 'oys6bls1rFf', 'pcq60feJ1', 'nLMV0NwBY', 'sfggegehhaw', 450000, '2021-04-30', '2021-06-09', 'Dalam Proses', 'bank_transfer', '2021-05-01 00:00:00', 'bca', '86214807628', 'https://app.sandbox.midtrans.com/snap/v1/transactions/56afb3d6-e06b-487e-9061-ffb7d45a2ef7/pdf', NULL, NULL, '2021-04-14 20:07:54', '2021-04-30 06:26:48'),
(31, '1w57dcLkxaS', '7G3ekFCv', 'nLMV0NwBY', 'sfggegehhaw', 450000, NULL, '2021-06-09', 'Belum Lunas', '', NULL, NULL, NULL, NULL, NULL, NULL, '2021-04-14 20:07:54', '2021-04-14 20:07:54'),
(32, 'ID10042021v8Qo', '38SMvxFX', 'nLMV0NwBY', 'sfggegehhaw', 450000, NULL, '2021-06-09', 'Belum Lunas', '', NULL, NULL, NULL, NULL, NULL, NULL, '2021-04-14 20:07:54', '2021-04-14 20:07:54'),
(56, 'oys6bls1rFf', 'zZBCxaGS', 'rRfWAzD7y', 'sfggegehhaw', 450000, NULL, '2021-07-17', 'Belum Bayar', NULL, NULL, NULL, NULL, NULL, '2021-06-18', NULL, '2021-06-17 23:12:13', '2021-06-17 23:12:13'),
(57, '1w57dcLkxaS', 'VfZG0inY', 'rRfWAzD7y', 'sfggegehhaw', 450000, NULL, '2021-07-19', 'Belum Bayar', NULL, NULL, NULL, NULL, NULL, '2021-06-18', NULL, '2021-06-17 23:12:13', '2021-06-17 23:12:13'),
(58, 'ID10042021v8Qo', 'aaefe9c0-c31c-471c-b9a5-85b968ce0ea5', 'rRfWAzD7y', 'sfggegehhaw', 450000, '2021-06-18', '2021-07-20', 'settlement', 'bank_transfer', '2021-06-19 13:13:16', 'bca', '86214912717', 'https://app.sandbox.midtrans.com/snap/v1/transactions/d89324cf-fa17-4ce0-8ee6-088f8abae7f0/pdf', '2021-06-18', NULL, '2021-06-17 23:12:13', '2021-07-14 19:33:34'),
(59, 'Wf0b2vd', 'UnD4Yjt6', 'rRfWAzD7y', 'sfggegehhaw', 450000, NULL, '2021-09-20', 'Belum Bayar', NULL, NULL, NULL, NULL, NULL, '2021-06-18', NULL, '2021-06-17 23:12:13', '2021-06-17 23:12:13'),
(76, 'oys6bls1rFf', '90413195-a9f0-44c0-8f3f-a8bb7f77494a', 'Nr25qn6dY', 'sfggegehhaw', 450000, '2021-07-18', '2021-11-14', 'Lunas', 'bank_transfer', '2021-07-19 08:30:30', 'bca', '86214167642', 'https://app.sandbox.midtrans.com/snap/v1/transactions/42f00204-4905-4330-8806-6ee06e6bc929/pdf', '2021-07-15', NULL, '2021-07-14 19:33:10', '2021-07-17 18:32:46'),
(77, '1w57dcLkxaS', 'jxx25pdV', 'Nr25qn6dY', 'sfggegehhaw', 450000, NULL, '2021-11-16', 'Belum Bayar', NULL, NULL, NULL, NULL, NULL, '2021-07-15', NULL, '2021-07-14 19:33:10', '2021-07-14 19:33:10'),
(78, 'ID10042021v8Qo', '5df5db5a-9e21-4eb4-b0dc-8ad2573dc23b', 'Nr25qn6dY', 'sfggegehhaw', 450000, '2021-07-15', '2021-11-17', 'settlement', 'bank_transfer', '2021-07-16 09:34:19', 'bca', '86214721662', 'https://app.sandbox.midtrans.com/snap/v1/transactions/fd63f7a1-8cf2-4105-b61d-1742bf94b75a/pdf', '2021-07-15', NULL, '2021-07-14 19:33:10', '2021-07-14 19:35:20'),
(79, 'Wf0b2vd', '1ixB8DS2', 'Nr25qn6dY', 'sfggegehhaw', 450000, NULL, '2022-01-18', 'Belum Bayar', NULL, NULL, NULL, NULL, NULL, '2021-07-15', NULL, '2021-07-14 19:33:10', '2021-07-14 19:33:10'),
(80, 'OYXQLFGlrKYj', 'g0cEDCof', 'Nr25qn6dY', 'sfggegehhaw', 450000, NULL, '2021-10-10', 'Belum Bayar', NULL, NULL, NULL, NULL, NULL, '2021-07-15', NULL, '2021-07-14 19:33:10', '2021-07-14 19:33:10'),
(81, 'oys6bls1rFf', 'PvrLtRhC', '6RgBhdoVh', 'sfggegehhaw', 450000, NULL, '2021-12-14', 'Belum Bayar', NULL, NULL, NULL, NULL, NULL, '2021-09-18', NULL, '2021-09-18 08:35:35', '2021-09-18 08:35:35'),
(82, '1w57dcLkxaS', 'OEi1qAZP', '6RgBhdoVh', 'sfggegehhaw', 450000, NULL, '2021-12-16', 'Belum Bayar', NULL, NULL, NULL, NULL, NULL, '2021-09-18', NULL, '2021-09-18 08:35:35', '2021-09-18 08:35:35'),
(83, 'ID10042021v8Qo', '2cd6921f-feec-4d04-a2b1-cf24e8d30d1b', '6RgBhdoVh', 'sfggegehhaw', 450000, '2021-09-18', '2021-12-17', 'Lunas', 'bank_transfer', '2021-09-19 22:39:45', 'bca', '86214231910', 'https://app.sandbox.midtrans.com/snap/v1/transactions/c0acf391-446a-4573-a906-797a3c139d14/pdf', '2021-09-18', NULL, '2021-09-18 08:35:35', '2021-09-18 08:42:33'),
(84, 'Wf0b2vd', 'B1VDub6E', '6RgBhdoVh', 'sfggegehhaw', 450000, NULL, '2022-02-17', 'Belum Bayar', NULL, NULL, NULL, NULL, NULL, '2021-09-18', NULL, '2021-09-18 08:35:35', '2021-09-18 08:35:35'),
(85, 'OYXQLFGlrKYj', '76FgiTvD', '6RgBhdoVh', 'sfggegehhaw', 450000, NULL, '2021-11-09', 'Belum Bayar', NULL, NULL, NULL, NULL, NULL, '2021-09-18', NULL, '2021-09-18 08:35:35', '2021-09-18 08:35:35'),
(86, 'oys6bls1rFf', 'hj2ifrh7', 'r4DGTANeY', 'sfggegehhaw', 450000, NULL, '2022-01-13', 'Belum Bayar', NULL, NULL, NULL, NULL, NULL, '2021-10-15', NULL, '2021-10-14 22:37:06', '2021-10-14 22:37:06'),
(87, '1w57dcLkxaS', 'bksI8oAL', 'r4DGTANeY', 'sfggegehhaw', 450000, NULL, '2022-01-15', 'Belum Bayar', NULL, NULL, NULL, NULL, NULL, '2021-10-15', NULL, '2021-10-14 22:37:06', '2021-10-14 22:37:06'),
(88, 'ID10042021v8Qo', 'bMW0s3qs', 'r4DGTANeY', 'sfggegehhaw', 450000, NULL, '2022-01-16', 'Belum Bayar', NULL, NULL, NULL, NULL, NULL, '2021-10-15', NULL, '2021-10-14 22:37:06', '2021-10-14 22:37:06'),
(89, 'Wf0b2vd', 'THVtZlFg', 'r4DGTANeY', 'sfggegehhaw', 450000, NULL, '2022-03-19', 'Belum Bayar', NULL, NULL, NULL, NULL, NULL, '2021-10-15', NULL, '2021-10-14 22:37:06', '2021-10-14 22:37:06'),
(90, 'OYXQLFGlrKYj', 's9wXrqLB', 'r4DGTANeY', 'sfggegehhaw', 450000, NULL, '2021-12-09', 'Belum Bayar', NULL, NULL, NULL, NULL, NULL, '2021-10-15', NULL, '2021-10-14 22:37:06', '2021-10-14 22:37:06');

-- --------------------------------------------------------

--
-- Table structure for table `pendapatan`
--

CREATE TABLE `pendapatan` (
  `id` int NOT NULL,
  `jumlah` int DEFAULT NULL,
  `id_bulan` varchar(15) DEFAULT NULL,
  `id_tahun` varchar(30) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `pendapatan`
--

INSERT INTO `pendapatan` (`id`, `jumlah`, `id_bulan`, `id_tahun`, `created_at`, `updated_at`) VALUES
(6, 450000, '2MoFnbmzB', 'sfggegehhaw', '2021-04-20 21:25:02', '2021-04-20 21:25:02'),
(7, 450000, '2MoFnbmzB', 'sfggegehhaw', '2021-04-20 21:35:36', '2021-04-20 21:35:36'),
(9, 450000, '3YP560QMu', 'sfggegehhaw', '2021-07-12 06:23:02', '2021-07-12 06:23:02'),
(10, 450000, '3YP560QMu', 'sfggegehhaw', '2021-07-12 07:01:14', '2021-07-12 07:01:14'),
(11, 450000, '3YP560QMu', 'sfggegehhaw', '2021-07-12 07:07:09', '2021-07-12 07:07:09'),
(12, 450000, '3YP560QMu', 'sfggegehhaw', '2021-07-12 07:12:17', '2021-07-12 07:12:17'),
(13, 450000, '3YP560QMu', 'sfggegehhaw', '2021-07-12 07:24:31', '2021-07-12 07:24:31'),
(14, 450000, 'Nr25qn6dY', 'sfggegehhaw', '2021-07-17 18:32:46', '2021-07-17 18:32:46'),
(15, 450000, '6RgBhdoVh', 'sfggegehhaw', '2021-09-18 08:42:33', '2021-09-18 08:42:33');

-- --------------------------------------------------------

--
-- Table structure for table `tahun`
--

CREATE TABLE `tahun` (
  `id` int NOT NULL,
  `id_tahun` varchar(11) NOT NULL,
  `tahun` year DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `tahun`
--

INSERT INTO `tahun` (`id`, `id_tahun`, `tahun`) VALUES
(1, 'sfggegehhaw', 2021);

-- --------------------------------------------------------

--
-- Table structure for table `userkeluar`
--

CREATE TABLE `userkeluar` (
  `id` int NOT NULL,
  `nama` varchar(20) DEFAULT NULL,
  `email` varchar(40) DEFAULT NULL,
  `id_user` varchar(20) DEFAULT NULL,
  `masuk` date DEFAULT NULL,
  `keluar` date DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `userkeluar`
--

INSERT INTO `userkeluar` (`id`, `nama`, `email`, `id_user`, `masuk`, `keluar`, `created_at`, `updated_at`) VALUES
(1, 'Anjar', NULL, 'wdfew12', '2021-04-04', '2021-04-30', '2021-04-15 04:23:27', '2021-04-15 04:23:27'),
(5, 'guest', 'guest@gmail.com', 'ID150420210BAO', '2021-04-15', '2021-04-15', '2021-04-14 21:39:52', '2021-04-14 21:39:52'),
(6, 'AbdillahAnjar', 'anjarabdillah122@gmail.com', '2lfHzt5H4uaC', '2021-07-12', '2021-07-12', '2021-07-12 07:28:00', '2021-07-12 07:28:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `id_user` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `role` int NOT NULL,
  `no_hp` varchar(30) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `id_user`, `role`, `no_hp`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@gmail.com', '2021-06-03 17:00:00', '$2y$10$dicK4ieacpTqRqng9c.Ds.oGrSuykIDw14cUykjqzTeVN1Ptj2SZO', NULL, '1617709969', 1, NULL, '2021-04-06 04:53:10', '2021-04-06 04:53:10'),
(2, 'Anjar', 'anjarabdillah3@gmail.com', NULL, '$2y$10$TkiJwfalz.spqR3AMerwtOuLW/6PtfIgKK9X1q.ZNLPa1RnFAWoOy', NULL, 'oys6bls1rFf', 2, '87778881849', '2022-01-13 07:02:47', '2021-10-14 22:37:06'),
(3, 'Adit', 'anasabdillah32@gmail.com', NULL, '$2y$10$7w9Xjdr8W8LFVZ/p.xNd0.2tou81li15e.fnEd6NuDRlJi.Dkfm4a', NULL, '1w57dcLkxaS', 2, '85228141227', '2022-01-14 18:45:00', '2021-10-14 22:37:06'),
(8, 'Anass', 'anjaranas2@gmail.com', NULL, '$2y$10$id6R8CBQc.4PmxyU0v9Gu.I.TZ13hHqirWN4OCfpQ2yDzd/Yq/TIe', NULL, 'ID10042021v8Qo', 2, NULL, '2022-01-15 18:11:37', '2021-10-14 22:37:06'),
(10, 'aku', 'aku@gmail.com', NULL, '$2y$10$ee48Xh7.s7T2LYg1x7K/huU5xhO5Dve1cnrXHEpTYyQOKRuADm87O', NULL, 'Wf0b2vd', 2, NULL, '2022-03-18 21:38:17', '2021-10-14 22:37:06'),
(14, 'Sugiyo', 'anjaranas21@gmail.com', NULL, '$2y$10$wVhFg8eyCx70LPVG7zFIbODCY.X425ecY72zPBN3NDWAso8uIRjKC', NULL, 'OYXQLFGlrKYj', 2, '087778881849', '2021-12-09 05:20:35', '2021-10-14 22:37:06');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bulan`
--
ALTER TABLE `bulan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `pelanggan`
--
ALTER TABLE `pelanggan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pembayaran`
--
ALTER TABLE `pembayaran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pendapatan`
--
ALTER TABLE `pendapatan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tahun`
--
ALTER TABLE `tahun`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `userkeluar`
--
ALTER TABLE `userkeluar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bulan`
--
ALTER TABLE `bulan`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `pelanggan`
--
ALTER TABLE `pelanggan`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `pembayaran`
--
ALTER TABLE `pembayaran`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;

--
-- AUTO_INCREMENT for table `pendapatan`
--
ALTER TABLE `pendapatan`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `tahun`
--
ALTER TABLE `tahun`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `userkeluar`
--
ALTER TABLE `userkeluar`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
